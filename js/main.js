"use strict;";

// you can change these values
const maxNumSnippets = 5; // Number of search results to return - int in 1..10
const maxNumImages = 9; // Number of image results

// set your credentials
const apiKey = "AIzaSyChrhPj6RBHjXrtlPkORI_TuBjUZhBdn4c";
const cseID = "017680411473609229632:5f9niqxhfav";


const form = document.querySelector(".searching form");
const input = document.querySelector(".searching input");
const alert = document.querySelector(".searching .alert");
const results = document.querySelector(".results");
// where result images will show
const nodeImages = results.querySelector(".flex-container-images");
// where result web-links will show
const nodeSnippets = results.querySelector(".snippets ul");

const mainUrl = `https://www.googleapis.com/customsearch/v1?`;
const maxResults = 100; // max number of results (100 is max in free google api)


// definiton of the class is in pagination.js
const pagination = new Pagination(
        maxResults,
        maxNumSnippets,
        1,
        document.getElementById("pagination"),
        10,
        document.querySelectorAll(".results .container")[1]
);
pagination.elContent = nodeSnippets; // remember where to replace content
const paginationImg = new Pagination(
    maxResults,
    maxNumImages,
    1,
    document.getElementById("pagination-img"),
    10,
    document.querySelectorAll(".results .container")[0]
);
paginationImg.elContent = nodeImages; // remember where to replace content
paginationImg.contentType = 'img'; // if it is image, for url fetch
const paginations = [pagination, paginationImg];    // array for better event handler


// fetch and show results
async function myFetch(url) {
    try {
        const response = await fetch(url);
        const data = await response.json();

        if (data.error) {
            throw data.error.message;
        }
        const { items } = data;
        if (!items) {
            throw "No results found.";
        }
        
        items.forEach(function (item) {
            if (item.image) {
                nodeImages.appendChild(makeImageElement(item));
            } else {
                nodeSnippets.appendChild(makeSnippetElement(item));
            }
        });
    } catch(err) {
        alert.querySelector(".error-msg").innerHTML = err;
        alert.classList.remove("hidden");
        results.classList.add("hidden");
    }
}

// close-button for error message
document.querySelector(".closebtn").addEventListener("click", e => {
    e.target.parentNode.classList.add("hidden");
});

// handle click on submit button
form.addEventListener("submit", e => {
    e.preventDefault();

    let query = encodeURIComponent(input.value);

    const snippetUrl = `${mainUrl}q=${query}&key=${apiKey}&cx=${cseID}&num=${maxNumSnippets}`;
    const imageUrl = `${mainUrl}q=${query}&key=${apiKey}&cx=${cseID}&num=${maxNumImages}&searchType=image`;

    // set default hide and show
    alert.classList.add("hidden");
    results.classList.remove("hidden");
    document.getElementById("loader").classList.remove("hidden");
    results.querySelector(".content").classList.add("hidden");

    nodeSnippets.innerHTML = "";
    nodeImages.innerHTML = "";
    paginations.forEach( item => {
        item.hide();
    });

    // wait for both of results
    Promise.all([myFetch(snippetUrl), myFetch(imageUrl)])
        .then(() => {
            document.getElementById("loader").classList.add("hidden");
            paginations.forEach( item => {
                item.render();
            });
            results.querySelector(".content").classList.remove("hidden");
        });
});

// handle page change - fetch another page
paginations.forEach( item => {
    item.element.addEventListener("click", e => {
        e.preventDefault();
    
        if (e.target.tagName !== "A" || ! e.target.hash) {
            return;
        }
    
        item.current = e.target.id;
    
        let startItem = e.target.dataset.firstItem;
        let num = item.itemsPerPage;
        if (e.target.dataset.getItems) {
            num = e.target.dataset.getItems;
        }
        let query = encodeURIComponent(input.value);
        let imageParam = "";
        if (item.contentType) {
            imageParam = "&searchType=image";
        }
        const url = `${mainUrl}q=${query}&key=${apiKey}&cx=${cseID}&num=${num}&start=${startItem}${imageParam}`;
        item.elPage.querySelector(".loader-small").classList.remove("hidden");
        item.elContent.classList.add("hidden");
        item.elContent.innerHTML = "";
        item.hide();
    
        myFetch(url).then(() => {
            item.elPage.querySelector(".loader-small").classList.add("hidden");
            item.elContent.classList.remove("hidden");
            item.show();
        });
    });
});
