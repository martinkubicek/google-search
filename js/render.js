"use strict;";

// functions for render items from search engine
const makeImageElement = (item) => {
    const { title, image } = item;
    const div = document.createElement("div");
    div.innerHTML = `
        <a href="${image.contextLink}" target="_blank">
        <img src="${image.thumbnailLink}"
            alt="${title}"
            title="${title}">
        </a>`;
    return div;
}
const makeSnippetElement = (item) => {
    const { title, link, displayLink, snippet } = item;
    const li = document.createElement("li");
    li.innerHTML = `
        <span class="title"><a href="${link}" target="_blank">${title}</a></span>
        <span class="url">${displayLink}</span>
        <span class="snippet">${snippet}</span>`;
    return li;
}