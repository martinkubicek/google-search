"use strict;";


/** Object with numbers of pages, current page. */
class Pagination {
    /**
    * @param  {number} maxItems - Maximum items
    * @param  {number} itemsPerPage - Maximum items on page
    * @param  {number} startPage - What page should be opened
    * @param  {HTMLElement} element - Element, where pagination will be inserted
    * @param  {number} maxPages - Maximum of pages
    * @param  {HTMLElement} page - Element with page content
    */
    constructor(maxItems, itemsPerPage, startPage, element, maxPages, page) {
        this.maxItems = maxItems;
        this.itemsPerPage = itemsPerPage;
        this._currentPage = startPage;
        this.element = element;
        this.pages = Math.ceil(this.maxItems / this.itemsPerPage);
        this.maxPages = maxPages;
        this.elPage = page;
    }

    /**
     * Set current page.
     * @param {number} current - The number of current page
     */
    set current (current) {
        this._currentPage = current;
        this.nodes.forEach(el => el.classList.remove("active"));
        this.nodes[this._currentPage - 1].classList.add("active");
    }

    /**
     * Get the current page.
     * @return {number} The number of current page.
     */
    get current() {
        return this._currentPage;
    }

    /**
     * Render pagination.
     */
    render() {
        this.element.innerHTML = "";
        this._currentPage = 1;
        let maxLeft = 0, minRight = 0; // "..." instead numbers if more pages
        if (this.pages > this.maxPages) {
            maxLeft = Math.floor(this.maxPages / 2) + 1;
            minRight = this.pages - maxLeft + 2;
        }
        let i = 1;
        for (i; i <= this.pages; i++ ) {
            let classes = "";
            if (i == maxLeft) {
                classes = "inactive";
            } else if (i > maxLeft && i < minRight) {
                classes = "hidden";
            }
            const page = document.createElement("a");
            page.classList.add("pagination-item");
            if (classes === "inactive") {
                page.id = "0";
                page.innerHTML = "...";
            } else {
                page.href = `#${i}`;
                page.id = i;
                let firstItem = (i - 1) * this.itemsPerPage + 1;
                page.dataset.firstItem = firstItem;
                if (firstItem + this.itemsPerPage >= this.maxItems) {
                    let getItems = this.maxItems - page.dataset.firstItem;
                    page.dataset.getItems = getItems;
                    if (! getItems) {
                        continue;
                    }
                }
                page.innerHTML = i;
                if (classes === "hidden") {
                    page.classList.add(classes);
                }
            }
            this.element.appendChild(page);
        }
        this.nodes = this.element.querySelectorAll(".pagination-item");
        this.nodes[this._currentPage - 1].classList.add("active");
        this.show();
    }

    /**
     * Hide pagination.
     */
    hide() {
        this.element.classList.add("hidden");
    }

    /**
     * Show pagination.
     */
    show() {
        this.element.classList.remove("hidden");
    }
}
